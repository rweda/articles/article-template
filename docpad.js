module.exports = {

  collections: {

    index: function() {
      return this
        .getCollection("html")
        .findAllLive({ outFilename: "index.html" })
        .on("add", (model) => model.setMetaDefaults({ layout: "article" }));
    },

  },

};
